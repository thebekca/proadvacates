import React from "react";
import styles from "./Forgot.module.scss";
import { NavLink } from "react-router-dom";
import Button from "../../components/Button/Button";
import { Back } from "../../components/Icons";
import { Input } from "antd";

const Forgot = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <NavLink to="/login">
          <Button icon={<Back />} title="Назад" />
        </NavLink>

        <div className={styles.content}>
          <div className={styles.form}>
            <h2 className={styles.title}>Восстановление пароля</h2>
            <div className={styles.main}>
              <p className={styles.subTitle}>
                Введите номер телефона, который привязан к аккаунту
              </p>

              <Input
                className={styles.input}
                prefix={"+998"}
                placeholder=" --  ---  -- --"
                allowClear
              />
            </div>
          </div>

          <div className={styles.item}>
            <Button title="Продолжить" variant="primary" size="four" />
          </div>
        </div>
      </div>

      <div>
        <img src="/Images/unsplash.png" alt="error" />
      </div>
    </div>
  );
};

export default Forgot;
