import React from "react";
import styles from "./SignUp.module.scss";
import { NavLink } from "react-router-dom";
import Button from "../../components/Button/Button";
import { Back, In, Instagramm, Facebook } from "../../components/Icons";
import { Input, Checkbox } from "antd";

const onChange = (e) => {
  console.log(`checked = ${e.target.checked}`);
};

const SignUp = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <div>
          <NavLink  to="/login">
            <Button icon={<Back />} title="Назад" />
          </NavLink>
        </div>

        <div className={styles.menu}>
          <div>
            <h2>Регистрация</h2>
          </div>

          <div className={styles.form}>
            <div className={styles.item}>
              <Input className={styles.input} placeholder="Ваше имя" />
              <Input className={styles.input} placeholder="Ваша почта" />
              <Input.Password
                className={styles.input}
                placeholder="Создать пароль"
              />
            </div>

            <div className={styles.item}>
              <Input className={styles.input} placeholder="Ваша фамилия" />
              <Input className={styles.input} prefix={"+998"} />
              <Input.Password
                className={styles.input}
                placeholder="Повторите пароль"
              />
            </div>
          </div>

          <div>
            <Checkbox onChange={onChange}>
              Я принимаю Условия использования
            </Checkbox>
          </div>

          <div className={styles.button}>
            <Button title="Зарегистрироваться" variant="primary" size="four" />
            <div className={styles.link}>
              Уже есть аккаунт?
              <NavLink to="/login">Войти</NavLink>
            </div>
          </div>
        </div>

        <div className={styles.network}>
          <h3 className={styles.subText}>Мы в социальных сетях</h3>
          <div className={styles.icons}>
            <a className={styles.insta} href="#">
              <Instagramm />
            </a>
            <a className={styles.in} href="#">
              <In />
            </a>
            <a className={styles.icon} href="#">
              <Facebook />
            </a>
          </div>
        </div>
      </div>

      <div>
        <img src="/Images/place.png" alt="error" />
      </div>
    </div>
  );
};

export default SignUp;
