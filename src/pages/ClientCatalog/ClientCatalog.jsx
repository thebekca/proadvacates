import React from "react";
import styles from "./ClientCatalog.module.scss";
import Catalog from "../../components/Catalog/Catalog";
import Topbar from "../../components/Topbar/Topbar";
import Footer from "../../components/Footer/Footer";

const ClientCatalog = () => {
  return (
    <div className={styles.wrapper}>
      <Topbar />

      <div className={styles.menu}>
        <div className={styles.list}>
          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="20 000 000 сум"
          />

          <Catalog
            title="Нужен юрист"
            subTitle="Simplex ITC"
            summ="5 000 000 сум"
            variant="secondry"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="8 000 000 сум"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="6 000 000 сум"
          />
        </div>

        <div className={styles.list}>
          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="20 000 000 сум"
          />

          <Catalog
            title="Нужен юрист"
            subTitle="Simplex ITC"
            summ="5 000 000 сум"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="8 000 000 сум"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="6 000 000 сум"
          />
        </div>

        <div className={styles.list}>
          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="20 000 000 сум"
          />

          <Catalog
            title="Нужен юрист"
            subTitle="Simplex ITC"
            summ="5 000 000 сум"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="8 000 000 сум"
          />

          <Catalog
            title="Нужен адвокат"
            subTitle="Simplex ITC"
            summ="6 000 000 сум"
          />
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default ClientCatalog;
