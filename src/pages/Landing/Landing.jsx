import React from "react";
import "./Landing.scss";
import Button from "../../components/Button/Button";
import {
  Hugo,
  Serficat,
  Contact,
  AboutUs,
  Data,
  Left,
  Right,
  Star,
  Spec,
  Done,
  Home,
  Person,
  Bag,
  Book,
  Bell,
} from "../../components/Icons";
import Servisce from "../../components/Servisce/Servisce";
import BookCard from "../../components/BookCard/BookCard";
import Menu from "../../components/Menu/Menu";
import { Swiper, SwiperSlide } from "swiper/react";
import { useCallback, useRef } from "react";
import { Pagination } from "swiper/modules";
import "swiper/css";
import "swiper/css/pagination";
import CourseCard from "../../components/CourseCard";
import MenuList from "../../components/MenuList/MenuList";
import { NavLink } from "react-router-dom";
import Link from "../../components/Link/Link";
import Topbar from "../../components/Topbar/Topbar";
import Footer from "../../components/Footer/Footer";

const Landing = () => {
  const navigationPrevRef = useRef(null);
  const navigationNextRef = useRef(null);
  const navigationPrevRef1 = useRef(null);
  const navigationNextRef1 = useRef(null);
  const navigationPrevRef2 = useRef(null);
  const navigationNextRef2 = useRef(null);

  const sliderRef = useRef();
  const sliderRef1 = useRef();
  const sliderRef2 = useRef();

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  const handlePrev1 = useCallback(() => {
    if (!sliderRef1.current) return;
    sliderRef1.current.swiper.slidePrev();
  }, []);

  const handleNext1 = useCallback(() => {
    if (!sliderRef1.current) return;
    sliderRef1.current.swiper.slideNext();
  }, []);

  const handlePrev2 = useCallback(() => {
    if (!sliderRef2.current) return;
    sliderRef2.current.swiper.slidePrev();
  }, []);

  const handleNext2 = useCallback(() => {
    if (!sliderRef2.current) return;
    sliderRef2.current.swiper.slideNext();
  }, []);

  function hideHorizontalScroll() {
    document.documentElement.style.overflowX = "hidden";
  }
  hideHorizontalScroll();

  return (
    <div className="app">
      <Topbar />

      <div className="content">
        <div className="contentHeader">
          <div className="contentText">
            <div className="contentTitle">
              Юридические услуги от
              <span className="contentSupTitle"> профессионалов</span>
            </div>
            <div className="contentDescrip">
              Получите квалифицированную юридическую помощь от наших опытных
              адвокатов!
            </div>

            <div>
              <Button
                title="Показать адвокатов"
                variant="primary"
                size="middle"
              />
            </div>
          </div>

          <div className="contentItem">
            <img className="contentImg" src="./Images/hammer.png" alt="error" />
          </div>
        </div>
      </div>

      <div className="menuList">
        <NavLink to="/">
          <MenuList icon={<Home />} title="Главная" active="active" />
        </NavLink>

        <NavLink to="advocates">
          <MenuList icon={<Person />} title="Адвокаты" />
        </NavLink>

        <NavLink to="services">
          <MenuList icon={<Bag />} title="Услуги" />
        </NavLink>

        <NavLink to="courses">
          <MenuList icon={<Book />} title="Курсы" />
        </NavLink>

        <NavLink to="">
          <MenuList icon={<Bell />} title="Уведомление" />
        </NavLink>
      </div>

      <div className="menu">
        <div className="menuWrapper">
          <div className="menuHeader">
            <h2 className="menuTitle">Адвокаты</h2>
            <div className="menuItem">
              <div onClick={handlePrev1} ref={navigationPrevRef1}>
                <div className="menuIndicator">
                  <Left />
                </div>
              </div>
              <div onClick={handleNext1} ref={navigationNextRef1}>
                <div className="menuIndicator">
                  <Right />
                </div>
              </div>
            </div>
          </div>

          <Swiper
            width={1366}
            height={true}
            className="swipper book-slider"
            ref={sliderRef1}
            slidesPerView={4}
            spaceBetween={24}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
          >
            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat1.svg"}
                name="Владимиров Александр"
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>
            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat2.svg"}
                name="Рустам Халиков "
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>
            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat3.svg"}
                name="Иванов Сергей"
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>
            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/morepraf.svg"}
                name="Море Прав"
                title="Юрид. компания"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat1.svg"}
                name="Владимиров Александр"
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat1.svg"}
                name="Владимиров Александр"
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <Menu
                icon={"./Images/advokat1.svg"}
                name="Владимиров Александр"
                title="Адвокат"
                num="4.5"
                starIcon={<Star />}
                serficatIcon={<Spec />}
                text="Стаж:"
                subText="8 лет"
                doneIcon={<Done />}
                doneTitle="Побед в суде: "
                doneNum="25 побед"
              />
            </SwiperSlide>
          </Swiper>

          <div className="menuContent">
            <div className="menuHeader">
              <h2 className="menuTitle">Адвокаты</h2>
              <div className="menuItem">
                <div
                  onClick={handlePrev2}
                  ref={navigationPrevRef2}
                  className="prev"
                >
                  <div className="menuIndicator">
                    <Left />
                  </div>
                </div>
                <div
                  onClick={handleNext2}
                  ref={navigationNextRef2}
                  className="next"
                >
                  <div className="menuIndicator">
                    <Right />
                  </div>
                </div>
              </div>
            </div>

            <Swiper
              width={1366}
              height={true}
              className="swipper book-slider"
              ref={sliderRef2}
              slidesPerView={4}
              spaceBetween={24}
              pagination={{
                clickable: true,
              }}
              modules={[Pagination]}
            >
              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat1.svg"}
                  name="Владимиров Александр"
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>
              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat2.svg"}
                  name="Рустам Халиков "
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>
              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat3.svg"}
                  name="Иванов Сергей"
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>
              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/morepraf.svg"}
                  name="Море Прав"
                  title="Юрид. компания"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>

              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat1.svg"}
                  name="Владимиров Александр"
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>

              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat1.svg"}
                  name="Владимиров Александр"
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>

              <SwiperSlide className="swiperSlide">
                <Menu
                  icon={"./Images/advokat1.svg"}
                  name="Владимиров Александр"
                  title="Адвокат"
                  num="4.5"
                  starIcon={<Star />}
                  serficatIcon={<Spec />}
                  text="Стаж:"
                  subText="8 лет"
                  doneIcon={<Done />}
                  doneTitle="Побед в суде: "
                  doneNum="25 побед"
                />
              </SwiperSlide>
            </Swiper>

            <NavLink to="advocates" className="menuButton">
              <Link title="Все адвокаты" />
            </NavLink>
          </div>
        </div>
      </div>

      <div className="servisce">
        <div className="servisceHeader">
          <div>
            <h1 className="servisceTitle">Услуги</h1>
          </div>

          <div className="servisceMenu">
            <Servisce
              icon={<Hugo />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
            <Servisce
              icon={<Serficat />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />

            <Servisce
              icon={<Contact />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
          </div>

          <NavLink to="services" className="servisceButton">
            <Link title="Все услуги" />
          </NavLink>
        </div>
      </div>

      <div className="books">
        <div className="booksHeader">
          <h2 className="booksTitle">
            Курсы <span className="booksSubTitle"> и книги</span>
          </h2>
          <div className="booksItem">
            <div
              className="booksIndicator"
              onClick={handlePrev}
              ref={navigationPrevRef}
            >
              <Left />
            </div>
            <div
              className="booksIndicator"
              onClick={handleNext}
              ref={navigationNextRef}
            >
              <Right />
            </div>
          </div>
        </div>
        <div className="booksMenu">
          <Swiper
            width={1365}
            height={true}
            className="swipper book-slider"
            ref={sliderRef}
            slidesPerView={4}
            spaceBetween={24}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
          >
            <SwiperSlide className="swiperSlide">
              <CourseCard
                icon={"./Images/compania.svg"}
                iconTitle="Море Прав"
                iconSubTitle="Юридическая компания"
                title="Курс на повышение квалификацией адвоката и юристов"
                dataIcon={<Data />}
                dataTitle="До 20.11.2023"
                summ="5 600 000 сум"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <CourseCard
                icon={"./Images/adliya.svg"}
                iconTitle="Ваш Юрист"
                iconSubTitle="Юридическая компания"
                title="Введение в юридическую профессию: основы адвокатуры"
                dataIcon={<Data />}
                dataTitle="До 20.11.2023"
                summ="5 600 000 сум"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <BookCard
                icon={"./Images/book.svg"}
                title="Юридические факты и их влияние на отраслевые институты права"
                subTitle="Монография посвящена анализу фактологической основы современного правового регулирования, роли юридических фактов в динамике институтов права в условиях социальных и технологических изменений."
                summ="12 600 000 сум"
              />
            </SwiperSlide>

            <SwiperSlide className="swiperSlide"></SwiperSlide>

            <SwiperSlide className="swiperSlide">
              <CourseCard
                icon={"./Images/adliya.svg"}
                iconTitle="Ваш Юрист"
                iconSubTitle="Юридическая компания"
                title="Введение в юридическую профессию: основы адвокатуры"
                dataIcon={<Data />}
                dataTitle="До 20.11.2023"
                summ="5 600 000 сум"
              />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>

      <div className="about">
        <div className="aboutItem">
          <h2 className="aboutTitle">О нас</h2>
          <p className="aboutSubTitle">
            “ProAdvocates” — это сайт юридических услуг, который предоставляет
            широкий спектр юридических услуг для защиты интересов клиентов. Наши
            опытные адвокаты готовы помочь в любых вопросах, связанных с
            гражданским, уголовным, административным, семейным, трудовым,
            наследственным и международным правом.
          </p>
        </div>

        <div className="aboutLogo">
          <AboutUs />
        </div>
      </div>

      <Footer />

      <div className="designedby">
        <p className="designedbyTitle">
          designed by <span className="designedText"> Simplex ITC</span>
        </p>
      </div>
    </div>
  );
};

export default Landing;
