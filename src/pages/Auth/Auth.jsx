import React from "react";
import styles from "./Auth.module.scss";
import Forma from "../../components/Forma/Forma";
import Button from "../../components/Button/Button";
import { Back, Instagramm, In, Facebook } from "../../components/Icons";
import { NavLink } from "react-router-dom";

const Auth = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <NavLink to="/">
          <Button icon={<Back />} title="Назад" />
        </NavLink>

        <div className={styles}>
          <Forma />
        </div>

        <div className={styles.network}>
          <h3 className={styles.subText}>Мы в социальных сетях</h3>
          <div className={styles.icons}>
            <a className={styles.insta} href="#">
              <Instagramm />
            </a>
            <a className={styles.in} href="#">
              <In />
            </a>
            <a className={styles.icon} href="#">
              <Facebook />
            </a>
          </div>
        </div>
      </div>
      <div className={styles.item}>
        <img src="/Images/photo.png" alt="" />
      </div>
    </div>
  );
};

export default Auth;
