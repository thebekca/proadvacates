import React from "react";
import styles from "./Courses.module.scss";
import CourseCard from "../../components/CourseCard";
import BookCard from "../../components/BookCard/BookCard";
import { Data } from "../../components/Icons";
import Footer from "../../components/Footer/Footer";
import Topbar from "../../components/Topbar/Topbar";

const Courses = () => {
  return (
    <div className={styles.wrapper}>
      <Topbar/>

      <div className={styles.menu}>
        <div className={styles.list}>
          <CourseCard
            icon={"./Images/compania.svg"}
            iconTitle="Море Прав"
            iconSubTitle="Юридическая компания"
            title="Курс на повышение квалификацией адвоката и юристов"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/compania.svg"}
            iconTitle="Море Прав"
            iconSubTitle="Юридическая компания"
            title="Курс на повышение квалификацией адвоката и юристов"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <BookCard
            icon={"./Images/book.svg"}
            title="Юридические факты и их влияние на отраслевые институты права"
            subTitle="Монография посвящена анализу фактологической основы современного правового регулирования, роли юридических фактов в динамике институтов права в условиях социальных и технологических изменений."
            summ="12 600 000 сум"
          />
        </div>

        <div className={styles.list}>
          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />
        </div>

        <div className={styles.list}>
          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />

          <CourseCard
            icon={"./Images/adliya.svg"}
            iconTitle="Ваш Юрист"
            iconSubTitle="Юридическая компания"
            title="Введение в юридическую профессию: основы адвокатуры"
            dataIcon={<Data />}
            dataTitle="До 20.11.2023"
            summ="5 600 000 сум"
          />
        </div>

      </div>

      <Footer/>
    </div>
  );
};

export default Courses;
