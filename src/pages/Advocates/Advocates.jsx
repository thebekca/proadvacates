import React from "react";
import styles from "./Advocates.module.scss";
import Menu from "../../components/Menu/Menu";
import Button from "../../components/Button/Button";
import {
  Star,
  Spec,
  Done,
} from "../../components/Icons";
import Footer from "../../components/Footer/Footer";
import Topbar from "../../components/Topbar/Topbar";

const Advocates = () => {
  return (
    <div className={styles.wrapper}>
      <Topbar/>

      <div className={styles.menu}>
        <div>
          <h2 className={styles.title}>Все адвокаты и компании</h2>
        </div>

        <div className={styles.list}>
          <Menu
            icon={"./Images/advokat6.svg"}
            name="Ибрагимов Улугбек"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/advokat2.svg"}
            name="Рустам Халиков "
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/advokat3.svg"}
            name="Иванов Сергей"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/morepraf.svg"}
            name="Море Прав"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />
        </div>

        <div className={styles.list}>
          <Menu
            icon={"./Images/advokat7.svg"}
            name="Гулямов Бегзод"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/advokat8.svg"}
            name="Исмоилов Саид"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/advokat9.svg"}
            name="Ибрагимов Ренат"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="13 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="50 побед"
          />

          <Menu
            icon={"./Images/advokat1.svg"}
            name="Владимиров Александр"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />
        </div>

        <div className={styles.list}>
          <Menu
            icon={"./Images/advokat10.svg"}
            name="Усманкулов Вадим"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />

          <Menu
            icon={"./Images/advokat4.svg"}
            name="Усманов Закир"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="15 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="65 побед"
          />

          <Menu
            icon={"./Images/advokat5.svg"}
            name="Алиев Мухаммад"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="13 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="50 побед"
          />

          <Menu  
            icon={"./Images/advokat11.svg"}
            name="Иванов Аскар"
            title="Адвокат"
            num="4.5"
            starIcon={<Star />}
            serficatIcon={<Spec />}
            text="Стаж:"
            subText="8 лет"
            doneIcon={<Done />}
            doneTitle="Побед в суде: "
            doneNum="25 побед"
          />
        </div>
      </div>

      <Footer/>

    </div>
  );
};

export default Advocates;
