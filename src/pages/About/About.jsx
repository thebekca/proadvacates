import React from 'react'
import styles from "./About.module.scss"
import Topbar from '../../components/Topbar/Topbar'
import Footer from '../../components/Footer/Footer'
import { AboutUs } from '../../components/Icons'

const About = () => {
  return (
    <div className={styles.wrapper}>
        <Topbar/>

        <div className={styles.about}>
            <div className={styles.item}>
                <h2 className={styles.title}>О нас</h2>
                <p className={styles.subTitle}>
                    “ProAdvocates” — это сайт юридических услуг, который предоставляет
                    широкий спектр юридических услуг для защиты интересов клиентов. Наши
                    опытные адвокаты готовы помочь в любых вопросах, связанных с
                    гражданским, уголовным, административным, семейным, трудовым,
                    наследственным и международным правом.
                </p>
            </div>

            <div className={styles.logo}>
                <AboutUs />
            </div>
        </div>

        <Footer/>
    </div>
  )
}

export default About