import React from "react";
import styles from "./AllServices.module.scss";
import Servisce from "../../components/Servisce/Servisce";
import Button from "../../components/Button/Button";
import {
  Hugo,
  Serficat,
  Contact,
} from "../../components/Icons";
import Topbar from "../../components/Topbar/Topbar";
import Footer from "../../components/Footer/Footer";

const AllServices = () => {
  return (
    <div className={styles.wrapper}>

      <Topbar />

      <div className={styles.services}>
        <div className={styles.content}>
          <div>
            <h2 className={styles.title}>Услуги</h2>
          </div>

          <div className={styles.menu}>
            <Servisce
              icon={<Hugo />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
            <Servisce
              icon={<Serficat />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />

            <Servisce
              icon={<Contact />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
          </div>

          <div className={styles.menu}>
            <Servisce
              icon={<Hugo />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
            <Servisce
              icon={<Serficat />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />

            <Servisce
              icon={<Contact />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
          </div>

          <div className={styles.menu}>
            <Servisce
              icon={<Hugo />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
            <Servisce
              icon={<Serficat />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />

            <Servisce
              icon={<Contact />}
              title="Защита прав потребителей"
              subTitle="Адвокаты оказывают услуги по защите прав потребителей, предоставляя юридическую помощь"
            />
          </div>
        </div>
      </div>

      <Footer />

    </div>
  );
};

export default AllServices;
