import React from "react";
import styles from "./Topbar.module.scss";
import Button from "../Button/Button";
import { Logo, Comment, Notification, SignIn, Chevron, Login } from "../Icons";
import { NavLink } from "react-router-dom";

const Topbar = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <div className={styles.item}>
          <a href="#">
            <Logo />
          </a>

          <div className={styles.menu}>
            <NavLink to="/catalog" className={styles.list}>
              Каталог клиентов
            </NavLink>

            <NavLink to="/services" className={styles.list}>
              Услуги
            </NavLink>

            <NavLink to="/courses" className={styles.list}>
              Курсы
            </NavLink>

            <NavLink to="/about" className={styles.list}>
              О нас
            </NavLink>
          </div>
        </div>

        <div className={styles.content}>
          <div className={styles.notificat}>
            <a href="#">
              <Comment />
            </a>
            <a className={styles.notificatIcon} href="#">
              <Notification />
            </a>
            <a className={styles.signInIcon} href="#">
              <SignIn />
            </a>
          </div>

          <div className={styles.lang}>
            <a className={styles.list} href="#">
              Ру
            </a>
            <div className={styles.chevron}>
              <Chevron />
            </div>
          </div>

          {/* <div className={styles.button}>
            <Button icon={<Login />} title="Войти" />
          </div> */}
          <NavLink to="/login" className={styles.button}>
            <Button icon={<Login />} title="Войти" />
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Topbar;
