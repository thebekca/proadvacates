import React from 'react'

const Contact = () => {
  return (
    <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
    <ellipse cx="9.99984" cy="6.66659" rx="3.33333" ry="3.33333" stroke="#D4B67F" stroke-width="2"/>
    <ellipse cx="10" cy="13.3333" rx="5" ry="3.33333" stroke="#D4B67F" stroke-width="2"/>
    <circle cx="29.9998" cy="26.6666" r="3.33333" stroke="#D4B67F" stroke-width="2"/>
    <path d="M36.6668 19.9999C36.6668 10.7952 29.2049 3.33325 20.0002 3.33325M20.0002 36.6666C10.7954 36.6666 3.3335 29.2047 3.3335 19.9999" stroke="#D4B67F" stroke-width="2" stroke-linecap="round"/>
    <ellipse cx="30" cy="33.3333" rx="5" ry="3.33333" stroke="#D4B67F" stroke-width="2"/>
    </svg>    
  )
}

export default Contact