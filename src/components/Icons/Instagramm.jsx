import React from 'react'

const Instagramm = () => {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="1.6665" y="1.66669" width="16.6667" height="16.6667" rx="4" stroke="#100E24" stroke-width="1.2"/>
    <ellipse cx="14.9998" cy="5.00002" rx="0.833333" ry="0.833333" fill="#100E24"/>
    <circle cx="10.0002" cy="9.99998" r="4.16667" stroke="#100E24" stroke-width="1.2"/>
    </svg>
  )
}

export default Instagramm