import React from 'react'

const Person = () => {
  return (
    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M9.90957 10.87C9.80957 10.86 9.68957 10.86 9.57957 10.87C7.19957 10.79 5.30957 8.84 5.30957 6.44C5.30957 3.99 7.28957 2 9.74957 2C12.1996 2 14.1896 3.99 14.1896 6.44C14.1796 8.84 12.2896 10.79 9.90957 10.87Z" stroke="#292D32" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M17.1603 4C19.1003 4 20.6603 5.57 20.6603 7.5C20.6603 9.39 19.1603 10.93 17.2903 11C17.2103 10.99 17.1203 10.99 17.0303 11" stroke="#292D32" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M4.90973 14.56C2.48973 16.18 2.48973 18.82 4.90973 20.43C7.65973 22.27 12.1697 22.27 14.9197 20.43C17.3397 18.81 17.3397 16.17 14.9197 14.56C12.1797 12.73 7.66973 12.73 4.90973 14.56Z" stroke="#292D32" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M19.0898 20C19.8098 19.85 20.4898 19.56 21.0498 19.13C22.6098 17.96 22.6098 16.03 21.0498 14.86C20.4998 14.44 19.8298 14.16 19.1198 14" stroke="#292D32" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>    
  )
}

export default Person