import React from 'react'

const Left = () => {
  return (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M15.0498 19.9201L8.5298 13.4001C7.7598 12.6301 7.7598 11.3701 8.5298 10.6001L15.0498 4.08008" stroke="#100E24" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>    
  )
}

export default Left