import React from 'react'

const Comment = () => {
  return (
    <svg width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M17.8332 4H15.1665C8.53909 4 3.1665 9.37258 3.1665 16V22.6667C3.1665 25.6122 5.55432 28 8.49984 28H17.8332C24.4606 28 29.8332 22.6274 29.8332 16C29.8332 9.37258 24.4606 4 17.8332 4Z" stroke="#28303F" stroke-width="1.5" stroke-linejoin="round"/>
    </svg>    
  )
}

export default Comment