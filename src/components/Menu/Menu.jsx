import React from "react";
import styles from "./Menu.module.scss";
import Button from "../Button/Button";
import Commentaria from "../Icons/Commentaria";

const Menu = ({
  icon,
  name,
  title,
  num,
  starIcon,
  serficatIcon,
  text,
  subText,
  doneIcon,
  doneTitle,
  doneNum,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
      <div className={styles.icon}>
        <img src={icon} alt="Error" />
      </div>

      <div className={styles.content}>
        <div className={styles.name}>{name}</div>
        <div className={styles.info}>
          <div className={styles.item}>
            <div className={styles.point}></div>
            <div className={styles.title}>{title}</div>
          </div>
          <div className={styles.rate}>
            <div className={styles.subTitle}>{num}</div>
            <div className={styles.icon}>{starIcon}</div>
          </div>
        </div>

        <div className={styles.info}>
          <div className={styles.item}>
            <div className={styles.icon}>{serficatIcon}</div>
            <div className={styles.title}>{text}</div>
          </div>
          <div className={styles.subTitle}>{subText}</div>
        </div>

        <div className={styles.info}>
          <div className={styles.item}>
            <div className={styles.icon}>{doneIcon}</div>
            <div className={styles.title}>{doneTitle}</div>
          </div>
          <div className={styles.subTitle}>{doneNum}</div>
        </div>
      </div>

      <div>
        <Button
          icon={<Commentaria />}
          title="Связаться по чату"
          variant="four"
          size="four"
        />
      </div>
      </div>
    </div>
  );
};

export default Menu;
