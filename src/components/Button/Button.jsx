import React from 'react'
import styles from "./Button.module.scss"
import cx from "classnames"

const Button = ({icon, title, variant, size}) => {
  return (
    <button className={cx(styles.wrapper, !!variant && styles[`wrapper--variant-${variant}`], !!size && styles[`wrapper--size-${size}`])}>
      <div className={styles.content}>
        <div className={styles.icon}>{icon}</div>
        <div className={styles.title}>{title}</div>
      </div>
    </button>
  )
}

export default Button