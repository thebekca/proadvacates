import React from "react";
import styles from "./Form.module.scss";
import { Form, Input } from "antd";
import Button from "../Button/Button";
import { NavLink } from "react-router-dom";

const onFinish = (values) => {
  console.log("Success:", values);
};
const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

const Forma = () => {
  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>Авторизация</h2>
      <Form
        style={{
          maxWidth: 300,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="username"
          rules={[
            {
              message: "Please input your username!",
            },
          ]}
        >
          <Input className={styles.input} placeholder="Username" />
        </Form.Item>

        <Form.Item
          name="password"
          rules={[
            {
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password className={styles.input} placeholder="Password" />
        </Form.Item>
      </Form>
      <div>
        <Button title="Войти" variant="primary" size="four" />
      </div>

      <div className={styles.link}>
        <NavLink to="/signUp" className={styles.signUp}>Регистрация</NavLink>
        <NavLink to="/forgotpassword" className={styles.forgot}>Забыли пароль?</NavLink>
      </div>
    </div>
  );
};

export default Forma;
