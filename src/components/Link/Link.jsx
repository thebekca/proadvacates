import React from 'react'
import styles from "./Link.module.scss"

const Link = ({title}) => {
  return (
    <div className={styles.wrapper}>
        <p className={styles.title}>{title}</p>
    </div>
  )
}

export default Link