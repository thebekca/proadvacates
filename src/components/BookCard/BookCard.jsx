import React from "react";
import styles from "./BookCard.module.scss";
import Button from "../Button/Button";

const BookCard = ({ icon, title, subTitle, summ }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <div>
          <img src={icon} alt="Error" />
        </div>
        <div className={styles.menu}>
          <div className={styles.title}>{title}</div>
          <div className={styles.subTitle}>{subTitle}</div>
          <div className={styles.summ}>{summ}</div>
        </div>
      </div>

      <div className={styles.button}>
        <Button title="Купить" variant="primary" size="sale" />
      </div>
    </div>
  );
};

export default BookCard;
