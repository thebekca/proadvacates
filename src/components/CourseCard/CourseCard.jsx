import React from "react";
import styles from "./CourseCard.module.scss";
import Button from "../Button/Button";

const CourseCard = ({
  icon,
  iconTitle,
  iconSubTitle,
  title,
  dataIcon,
  dataTitle,
  summ,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.picture}>
        <div>
          <img src={icon} alt="Error" />
        </div>
        <div className={styles.info}>
          <h1 className={styles.iconTitle}>{iconTitle}</h1>
          <p className={styles.iconSubTitle}>{iconSubTitle}</p>
        </div>
      </div>

      <div className={styles.content}>
        <div className={styles.menu}>
          <div className={styles.title}>{title}</div>
          <div className={styles.data}>
            <div className={styles.dataIcon}> {dataIcon} </div>
            <div className={styles.dataTitle}>{dataTitle}</div>
          </div>
        </div>
        <div className={styles.summ}>{summ}</div>
      </div>

      <div>
        <Button title="Подать заявку" variant="third" size="long" />
      </div>
    </div>
  );
};

export default CourseCard;
