import React from "react";
import styles from "./Servisce.module.scss";
import Button from "../Button/Button";

const Servisce = ({ icon, title, subTitle }) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.icon}>{icon}</div>
      <div className={styles.text}>
        <div className={styles.title}>{title}</div>
        <div className={styles.subTitle}>{subTitle}</div>
      </div>
      <div>
        <Button title="Подробнее" variant="secondry" size="strong" />
      </div>
    </div>
  );
};

export default Servisce;
