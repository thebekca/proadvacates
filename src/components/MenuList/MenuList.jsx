import React from 'react'
import styles from "./MenuList.module.scss"
import cx from "classnames"

const MenuList = ({icon, title, active}) => {
  return (
    <div className={cx(styles.wrapper, !!active && styles[`wrapper--active-${active}`])}>
        <div className={styles.icon}>{icon}</div>
        <p className={styles.title}>{title}</p>
    </div>
  )
}

export default MenuList