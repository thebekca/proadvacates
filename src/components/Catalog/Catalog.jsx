import React from "react";
import styles from "./Catalog.module.scss";
import cx from "classnames"
import Button from "../Button/Button";
import Go from "../Icons/Go";

const Catalog = ({ title, subTitle, summ, variant}) => {
  return (
    <div className={cx(styles.wrapper, !!variant && styles[`wrapper--variant-${variant}`])}>
      <div className={styles.item}>
        <h2 className={styles.title}>{title}</h2>
        <p className={styles.subTitle}>{subTitle}</p>
      </div>
      <div className={styles.buy}>
        <p className={styles.summ}>{summ}</p>
      </div>
      <div>
        <Button
          icon={<Go />}
          title="Перейти"
          variant="primary"
          size="four"
        />
      </div>
    </div>
  );
};

export default Catalog;
