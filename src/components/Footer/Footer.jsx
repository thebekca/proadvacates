import React from 'react'
import styles from "./Footer.module.scss"
import { Logo, Instagramm, In, Facebook } from '../Icons'
import { NavLink } from 'react-router-dom'

const Footer = () => {
  return (
    <div className={styles.wrapper}>
        <div className={styles.content}>
          <div>
            <Logo />
          </div>
          <div className={styles.title}>© 2024 OOO «Proadvocates»</div>
        </div>

        <div className={styles.menu}>
          <div className={styles.list}>
            <NavLink to="/catalog" className={styles.link}>
              Каталог клиентов
            </NavLink>

            <NavLink to="/services" className={styles.link}>
              Услуги
            </NavLink>

            <NavLink to="/about" className={styles.link}>
              О нас
            </NavLink>

            <NavLink to="/services" className={styles.link}>
              Услуги
            </NavLink>

          </div>
          <div className={styles.itc}>
            <p className={styles.subTitle}>
              designed by <span className="footerText">Simplex ITC</span>
            </p>
          </div>
        </div>

        <div className={styles.network}>
          <h3 className={styles.subText}>Социальные сети</h3>
          <div className={styles.icons}>
            <a className={styles.insta} href="#">
              <Instagramm />
            </a>
            <a className={styles.in} href="#">
              <In />
            </a>
            <a className={styles.icon} href="#">
              <Facebook />
            </a>
          </div>
        </div>
    </div>
  )
}

export default Footer