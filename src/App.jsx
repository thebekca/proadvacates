import "./App.scss";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Landing from "./pages/Landing/Landing";
import AllServices from "./pages/AllServices/AllServices";
import Advocates from "./pages/Advocates/Advocates";
import Catalog from "./pages/ClientCatalog/ClientCatalog";
import Courses from "./pages/Courses/Courses";
import About from "./pages/About/About";
import Auth from "./pages/Auth/Auth";
import SignUp from "./pages/SignUp/SignUp";
import Forgot from "./pages/Forgot/Forgot";

function App() {
  const routes = createBrowserRouter([
    {
      path: "/",
      element: <Landing />,
    },
    {
      path: "services",
      element: <AllServices />,
    },
    {
      path: "advocates",
      element: <Advocates />,
    },
    {
      path: "catalog",
      element: <Catalog />,
    },
    {
      path: "courses",
      element: <Courses />,
    },
    {
      path: "about",
      element: <About />,
    },
    {
      path: "login",
      element: <Auth />,
    },
    {
      path: "signUp",
      element: <SignUp />,
    },
    {
      path: "forgotpassword",
      element: <Forgot />,
    },
  ]);

  return (
    <div className="app">
      <RouterProvider router={routes} />
    </div>
  );
}

export default App;
